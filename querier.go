package bungeecordQuerier

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
)

type ShortResponse struct {
	Motd        string `json:"motd"`
	GameType    string `json:"gameType"`
	WorldName   string `json:"worldName"`
	OnlineCount int    `json:"onlineCount"`
	MaxPlayer   int    `json:"maxPlayer"`
	Port        int    `json:"port"`
	Ip          string `json:"ip"`
}

type LongResponse struct {
	Hostname    string   `json:"hostname"`
	GameType    string   `json:"gameType"`
	GameId      string   `json:"gameId"`
	Version     string   `json:"version"`
	Plugins     string   `json:"plugins"`
	WorldName   string   `json:"worldName"`
	OnlineCount int      `json:"onlineCount"`
	MaxPlayer   int      `json:"maxPlayer"`
	Port        int      `json:"port"`
	Ip          string   `json:"ip"`
	Players     []string `json:"players"`
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func toByteArray(i int) []byte {
	arr := make([]byte, 4)
	binary.BigEndian.PutUint32(arr[0:4], uint32(i))
	return arr
}

func generateIDToken() []byte {
	return toByteArray(rand.Intn(int(math.Pow(2, 31))) & 0x0F0F0F0F)
}

func compareSessionId(id []byte, received []byte) bool {
	if bytes.Equal(id, received) {
		return true
	} else {
		return false
	}
}

func getChallengeToken(id []byte, conn net.Conn, ignoreMismatch bool) []byte {
	challengeRequest := make([]byte, 3)
	// Magic byte
	challengeRequest[0] = 0xFE
	challengeRequest[1] = 0xFD
	// Challenge type
	challengeRequest[2] = 0x09
	challengeRequest = append(challengeRequest, id...)
	_, err := conn.Write(challengeRequest)
	checkErr(err)

	// 1 byte Challenge type + 4 byte id token + 11 bytes new challenge token (int 32 value -2147483648 - 2147483647) + 1 0x00 padding
	challengeRes := make([]byte, 17)

	size, err := conn.Read(challengeRes)
	checkErr(err)

	if !compareSessionId(id, challengeRes[1:5]) {
		if ignoreMismatch {
			fmt.Println("Session ID mismatch but continue anyway")
		} else {
			panic("Session ID mismatch")
		}
	}

	var res = &bytes.Buffer{}
	res.Write(challengeRes[5:size])

	res2, err := res.ReadBytes(0x00)
	checkErr(err)

	challengeTokenInt, err := strconv.Atoi(string(res2[:len(res2)-1]))
	checkErr(err)

	return toByteArray(challengeTokenInt)
}

func shortResponse(id []byte, conn net.Conn, challengeToken []byte, ignoreMismatch bool, decoder *encoding.Decoder) ShortResponse {
	queryRequest := make([]byte, 3)
	// Magic byte
	queryRequest[0] = 0xFE
	queryRequest[1] = 0xFD
	// Challenge type
	queryRequest[2] = 0x00

	queryRequest = append(queryRequest, id...)
	queryRequest = append(queryRequest, challengeToken...)
	_, err := conn.Write(queryRequest)
	checkErr(err)

	queryResponse := make([]byte, 200)
	size, err := conn.Read(queryResponse)
	checkErr(err)

	if !compareSessionId(id, queryResponse[1:5]) {
		if ignoreMismatch {
			fmt.Println("Session ID mismatch but continue anyway")
		} else {
			panic("Session ID mismatch")
		}
	}
	res := bytes.NewBuffer(queryResponse[5:size])

	response := ShortResponse{}
	for i := 0; i < 7; i++ {
		res2, err := res.ReadBytes(0x00)
		if errors.Is(err, io.EOF) {
			break
		}
		checkErr(err)
		var tmp []byte
		if i != 5 {
			tmp, err = decoder.Bytes(res2)
		} else {
			tmp = res2
		}
		checkErr(err)
		switch i {
		case 0:
			response.Motd = string(tmp)
		case 1:
			response.GameType = string(tmp)
		case 2:
			response.WorldName = string(tmp)
		case 3:
			onlineCount, err := strconv.ParseInt(string(tmp[:len(tmp)-1]), 10, 0)
			checkErr(err)
			response.OnlineCount = int(onlineCount)
		case 4:
			maxPlayer, err := strconv.ParseInt(string(tmp[:len(tmp)-1]), 10, 0)
			checkErr(err)
			response.MaxPlayer = int(maxPlayer)
		case 5:
			port := binary.LittleEndian.Uint16(tmp[:2])
			response.Port = int(port)
			response.Ip = string(tmp[2:])
		}
	}

	return response
}

func longResponse(id []byte, conn net.Conn, challengeToken []byte, ignoreMismatch bool, decoder *encoding.Decoder) LongResponse {
	queryRequest := make([]byte, 3)
	// Magic byte
	queryRequest[0] = 0xFE
	queryRequest[1] = 0xFD
	// Challenge type
	queryRequest[2] = 0x00

	queryRequest = append(queryRequest, id...)
	queryRequest = append(queryRequest, challengeToken...)
	// 4 bytes padding for long response
	queryRequest = append(queryRequest, id...)
	_, err := conn.Write(queryRequest)
	checkErr(err)

	queryResponse := make([]byte, 65535)
	size, err := conn.Read(queryResponse)
	checkErr(err)

	if !compareSessionId(id, queryResponse[1:5]) {
		if ignoreMismatch {
			fmt.Println("Session ID mismatch but continue anyway")
		} else {
			panic("Session ID mismatch")
		}
	}

	// 1 byte type + 4 bytes session id + 11 meaningless bytes
	res := bytes.NewBuffer(queryResponse[5+len("splitnum")+1+1+1 : size])

	response := LongResponse{}
forLoop:
	for {
		key, err := res.ReadBytes(0x00)
		// normally shd not happen due to having padding and players section afterward
		if errors.Is(err, io.EOF) {
			break
		}
		checkErr(err)
		value, err := res.ReadBytes(0x00)
		// normally shd not happen due to having padding and players section afterward
		if errors.Is(err, io.EOF) {
			break
		}
		checkErr(err)
		tmp, err := decoder.Bytes(value[:len(value)-1])
		checkErr(err)

		switch strings.ToLower(string(key[:len(key)-1])) {
		case "hostname":
			response.Hostname = string(tmp)
		case "gametype":
			response.GameType = string(tmp)
		case "game_id":
			response.GameId = string(tmp)
		case "version":
			response.Version = string(tmp)
		case "plugins":
			response.Plugins = string(tmp)
		case "map":
			response.WorldName = string(tmp)
		case "numplayers":
			onlineCount, err := strconv.ParseInt(string(tmp), 10, 0)
			checkErr(err)
			response.OnlineCount = int(onlineCount)
		case "maxplayers":
			maxPlayer, err := strconv.ParseInt(string(tmp), 10, 0)
			checkErr(err)
			response.MaxPlayer = int(maxPlayer)
		case "hostport":
			port, err := strconv.ParseInt(string(tmp), 10, 0)
			checkErr(err)
			response.Port = int(port)
		case "hostip":
			response.Ip = string(tmp)
			break forLoop
		}
	}

	// 1 0x00 padding after key value pair + 0x01 + player_ + 2 meaningless 0x00
	res.Next(1 + 1 + len("player_") + 1 + 1)
	var players []string
	for {
		value, err := res.ReadBytes(0x00)
		// normally shd not happen due to having padding and players section afterward
		if errors.Is(err, io.EOF) {
			break
		}
		checkErr(err)
		if len(value) == 1 {
			continue
		}
		tmp, err := decoder.Bytes(value[:len(value)-1])
		checkErr(err)
		players = append(players, string(tmp))
	}
	response.Players = players

	return response
}

func GetShortResponse(ip string, ignoreMismatch bool) ShortResponse {
	decoder := charmap.ISO8859_1.NewDecoder()

	rand.Seed(time.Now().UnixNano())

	// session id token
	id := generateIDToken()
	conn, err := net.DialTimeout("udp", ip, 5*time.Second)
	checkErr(err)

	err = conn.SetDeadline(time.Now().Add(10 * time.Second))
	checkErr(err)

	defer conn.Close()

	// challengeToken for querying
	challengeToken := getChallengeToken(id, conn, ignoreMismatch)
	// querying without padding for short response
	queryResponse := shortResponse(id, conn, challengeToken, ignoreMismatch, decoder)

	return queryResponse
}

func GetLongResponse(ip string, ignoreMismatch bool) LongResponse {
	decoder := charmap.ISO8859_1.NewDecoder()

	rand.Seed(time.Now().UnixNano())

	// session id token
	id := generateIDToken()
	conn, err := net.DialTimeout("udp", ip, 5*time.Second)
	checkErr(err)

	err = conn.SetDeadline(time.Now().Add(10 * time.Second))
	checkErr(err)

	defer conn.Close()

	// challengeToken for querying
	challengeToken := getChallengeToken(id, conn, ignoreMismatch)
	// querying with padding for long response
	queryResponse := longResponse(id, conn, challengeToken, ignoreMismatch, decoder)

	return queryResponse
}
